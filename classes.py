class Position:
    def __init__(self, x, y, z):
        self.x = x
        self.y = y
        self.z = z
        self.position = (x, y, z)

    def off(self, x, y, z):
        """Return a new Position object with position set to
        offset from the current position.
        """
        return Position(self.x + x, self.y + y, self.z + z)


class Portal:
    """The state of cell at `position` is set to `state`
    whenever the function `func` returns True for `conditions`.
    """

    def __init__(self, position, state, *conditions, func):
        self.position = position
        self.state = state
        self.conditions = conditions
        self.func = func

        # If the func returned True, then update the cell.
        self.to_port = False

    def check(self, board):
        # Update board parameter for all conditions
        for condition in self.conditions:
            condition.board = board
        self.to_port = self.func(self.conditions)

    def port(self, board):
        if self.to_port:
            board.get_cell(self.position).state = self.state
            self.to_port = False


class Condition:
    """Conditions evaluate to or their value is set to `True`
    when the state of cell at `position` is present in the `states`
    or when `states` also contains state of the cell at `position`.
    """

    def __init__(self, position, *states, board=None):
        self.position = position
        self.states = states
        self.board = board

    def get_value(self, board=None):
        return (
            self.board.get_cell(self.position).state in self.states
            if board is None
            else board.get_cell(self.position).state in self.states
        )


class Cell:
    """Cells are the fundamental unit of any Universe / Board.
    They have a particular predefined `state`,
    and they can also hold several `portals` that can alter this state.
    """

    def __init__(self, state, portals=None):
        self.state = state
        self.portals = [] if portals is None else portals

    def set_portals(self, *portals):
        self.portals += portals


class Board:
    """Board or Universe is contains all possible cells.
    (x, y, z) are the dimensions of the board.
    """

    def __init__(self, _x, _y=1, _z=1, default_state=None):
        self.x = _x
        self.y = _y
        self.z = _z
        self.wrap = True

        self.board = [
            [[Cell(default_state) for _ in range(self.x)] for _ in range(self.y)]
            for _ in range(self.z)
        ]

    def get_cell(self, position, wrap=None, default=None):
        """Return cell object at `position` on `board`.
        If position doesn't exist on board,
        then try wrapping if `wrap` is set to True,
        else return the `default` argument provided.
        """
        x, y, z = position.position
        try:
            return (
                self.board[z % self.z][y % self.y][x % self.x]
                if wrap is True or self.wrap is True
                else self.board[z][y][x]
            )
        except IndexError:
            return default
