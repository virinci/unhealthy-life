import curses
from life import Life
from classes import Position
import time
import patterns


def init_colors():
    """Maps integers 10 to 17 to terminal colors 0 to 7.
    10 to 17 is used insted of 0 to 7 because 0 has default value in curses,
        and value of 0 cannot be changed.
    This function is created to avoid writing same code in every module
        that requires colors.
    """
    colors = [
        curses.COLOR_BLACK,
        curses.COLOR_RED,
        curses.COLOR_GREEN,
        curses.COLOR_YELLOW,
        curses.COLOR_BLUE,
        curses.COLOR_MAGENTA,
        curses.COLOR_CYAN,
        curses.COLOR_WHITE,
    ]

    for i, j in enumerate(colors, start=10):
        curses.init_pair(i, j, curses.COLOR_BLACK)


def show(stdscr, life):
    # stdscr.clear()
    for z, y, x in life.iter:
        stdscr.addstr(
            y,
            2 * x,
            2 * chr(0x2588),
            curses.color_pair(
                13 if life.board.get_cell(Position(x, y, z)).state == Life.ALIVE else 10
            ),
        )
    # stdscr.refresh()


def main(stdscr):
    curses.curs_set(0)
    # curses.raw()
    stdscr.clear()
    stdscr.nodelay(True)
    stdscr.timeout(0)
    init_colors()

    y = x = 0
    generations = 0
    paused = False
    nextframe = False

    while True:
        ny, nx = stdscr.getmaxyx()
        if (nx, ny) != (x, y):
            x, y = nx, ny
            life = Life(x // 2 - 1, y - 1)
            life.update(patterns.GUN)

        key = stdscr.getch()

        if key == 27:
            break
        elif key == 32:
            paused = not paused
        elif key == curses.KEY_RIGHT and paused:
            nextframe = True

        if not paused or nextframe:
            try:
                show(stdscr, life)
            except curses.error:
                continue
            life.check()
            life.port()
            nextframe = False
            generations += 1

    return generations


generations = curses.wrapper(main)
print(generations)

"""
life = Life(60, 30)
life.update(patterns.GUN)

while True:
    life.show()
    life.check()
    life.port()
    # time.sleep(0.1)
"""
