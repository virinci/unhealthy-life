import sys
from color import ANSI
from classes import Position, Condition, Board, Portal
import itertools as it


class Life:
    DEAD = 0
    ALIVE = 1

    def __init__(self, _x, _y):
        self.board = Board(_x, _y, 1, Life.DEAD)
        self.iter = tuple(
            it.product(range(self.board.z), range(self.board.y), range(self.board.x))
        )

        # Setting up portals
        for z, y, x in self.iter:
            position = Position(x, y, z)
            curr = Condition(position, Life.ALIVE)
            neigh = [
                Condition(position.off(i, j, 0), Life.ALIVE)
                for i, j, k in set(Life.offsets())
            ]

            # Adding 4 portals. underpopulation, overpopulation, stay alive, born
            self.board.get_cell(position).set_portals(
                Portal(
                    position,
                    Life.DEAD,
                    curr,
                    *neigh,
                    func=lambda conds: conds[0].get_value()
                    and sum(i.get_value() for i in conds[1:]) < 2
                ),
                Portal(
                    position,
                    Life.DEAD,
                    curr,
                    *neigh,
                    func=lambda conds: conds[0].get_value()
                    and sum(i.get_value() for i in conds[1:]) > 3
                ),
                Portal(
                    position,
                    Life.ALIVE,
                    curr,
                    *neigh,
                    func=lambda conds: conds[0].get_value()
                    and sum(i.get_value() for i in conds[1:]) in (2, 3)
                ),
                Portal(
                    position,
                    Life.ALIVE,
                    curr,
                    *neigh,
                    func=lambda conds: not conds[0].get_value()
                    and sum(i.get_value() for i in conds[1:]) == 3
                ),
            )

    def update(self, positions):
        """Toggle cells at given positions."""
        for position in positions:
            position = Position(*position)
            if self.board.get_cell(position).state == Life.ALIVE:
                self.board.get_cell(position).state = Life.DEAD
            else:
                self.board.get_cell(position).state = Life.ALIVE

    @staticmethod
    def offsets():
        """
        # For 3D
        return [
            (i, j, k)
            for i in range(-1, 2)
            for j in range(-1, 2)
            for k in range(-1, 2)
            if not i == j == k == 0
        ]
        """
        return [
            (i, j, 0) for i in range(-1, 2) for j in range(-1, 2) if not i == j == 0
        ]

    def show(self):
        sys.stdout.write(ANSI.clear)
        p_y, p_z = 0, 0

        for z, y, x in self.iter:
            sys.stdout.write("\n\n" if z != p_z else "\n" if y != p_y else "")
            sys.stdout.write(
                (
                    ANSI.yellow
                    if self.board.get_cell(Position(x, y, z)).state == Life.ALIVE
                    else ANSI.black
                )
                + chr(0x2588) * 2
                + ANSI.reset
            )
            p_y, p_z = y, z

    def check(self):
        for z, y, x in self.iter:
            for p in self.board.get_cell(Position(x, y, z)).portals:
                p.check(self.board)

    def port(self):
        for z, y, x in self.iter:
            for p in self.board.get_cell(Position(x, y, z)).portals:
                p.port(self.board)
